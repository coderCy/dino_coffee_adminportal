import './App.scss'
import Dashboard from './Components/Dashboard/Dashboard'
import Login from './Components/Login/Login'
import Register from './Components/Register/Register'

function App() {
  
   return (
    <div>
      <Dashboard/>
      <Login/>
      <Register/>
    </div>
   )   
}  
export default App
